class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :room

  default_scope -> { order(:created_at) }
  scope :recent, ->(limit) { last(limit) }

  validates :user_id, :room_id, presence: true
  validates :content, presence: true

  set_callback :create, :after, :post_to_room

  private

  def post_to_room
    room.send_message(:new, attributes, namespace: :message)
  end
end
