class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable, :validatable, :confirmable, :lockable, :timeoutable

  has_many :rooms, inverse_of: :owner, foreign_key: :owner_id,
                   dependent: :nullify
  has_many :memberships, inverse_of: :user, class_name: 'Member',
                         dependent: :destroy
  has_many :messages, inverse_of: :user, dependent: :nullify

  validates :name, presence: true

  def joined(room)
    room.send_message(:joined, self, namespace: :user)
  end
end
