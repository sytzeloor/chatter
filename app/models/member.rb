class Member < ActiveRecord::Base
  belongs_to :user, counter_cache: true, inverse_of: :memberships
  belongs_to :room, counter_cache: true, inverse_of: :members

  validates :user_id, uniqueness: { scope: [:room_id] }
end
