class Room < ActiveRecord::Base
  belongs_to :owner, inverse_of: :rooms, class_name: 'User'
  has_many :members, inverse_of: :room, dependent: :destroy
  has_many :messages, inverse_of: :room, dependent: :destroy

  validates :owner_id, presence: true
  validates :name, presence: true

  def send_message(event, message, opts = {})
    Room.send_message(id, event, message, opts)
  end

  def self.send_message(id, event, message, opts = {})
    WebsocketRails["room_#{id}"].trigger(event, message, opts)
  end

  def send_delayed_message(delay, event, message, opts = {})
    Resque.enqueue_in(delay, DelayedMessageWorker, id, event, message, opts)
  end
end
