class ApplicationPresenter < ActionPresenter::Base
  def localize_time(time, format = :short)
    if object.respond_to?(time)
      if object.send(time).nil?
        '-'
      else
        I18n.l(object.send(time), format: format)
      end
    else
      '-'
    end
  end

  def dom_id
    h.dom_id(object)
  end

  def method_missing(*args, &block)
    begin
      super
    rescue
      object.send(*args, &block)
    end
  end
end
