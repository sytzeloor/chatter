# require 'resque/plugins/heroku'

class BaseWorker
  # extend Resque::Plugins::Heroku

  class LogColorError < StandardError; end

  def self.log(message, color = 'blue')
    return unless Rails.logger.level == 0

    %w(black red green yellow blue magenta cyan white).include?(color)

    Rails.logger.debug("[#{self.name}] ".send(color) + message)
  end
end
