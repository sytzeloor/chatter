class DelayedMessageWorker < BaseWorker
  @queue = :critical

  def self.perform(room_id, event, message, opts = {})
    Room.send_message(room_id, event, message, opts = {})
  end
end
