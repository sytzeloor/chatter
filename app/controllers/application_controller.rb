require 'application_responder'

class ApplicationController < ActionController::Base
  after_filter :allow_cross_domain, if: :api_request?

  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def api_request?
    request.format == 'json'
  end

  def options
    if request.method == :options
      allow_cross_domain
      render text: '', content_type: 'text/plain', layout: false, template: false
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  private

  def allow_cross_domain
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = %w(OPTIONS GET POST PUT DELETE).join(',')
    headers['Access-Control-Allow-Headers'] = %w(Origin Accept Content-Type X-Requested-With X-CSRF-Token).join(',')
    headers['Access-Control-Max-Age'] = '86400'
  end
end
