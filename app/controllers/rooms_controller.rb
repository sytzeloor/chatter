class RoomsController < ApplicationController
  respond_to :json, only: [:index]

  before_action :load_resource
  authorize_resource

  def index
    respond_with @rooms
  end

  def show
    respond_with @room
  end

  def new
    respond_with @room
  end

  def edit
    respond_with @room
  end

  def create
    @room.save
    respond_with @room
  end

  def update
    @room.update_attributes(permitted_params)
    respond_with @room
  end

  def destroy
    @room.destroy
    respond_with @room, location: rooms_path
  end

  private

  def load_resource
    case params[:action].to_sym
    when :index
      @rooms = Room.accessible_by(current_ability)
    when :new
      @room = Room.new
    when :create
      @room = current_user.rooms.build(permitted_params)
    else
      @room = Room.find(params[:id])
    end
  end

  def permitted_params
    params.require(:room).permit(:name, :description)
  end
end
