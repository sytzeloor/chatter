module Sockets
  class MessagesController < WebsocketRails::BaseController
    def create
      @room = Room.find(params[:room_id])
      @message = @room.messages.build({ content: message, user_id: User.first.id })

      if @message.save
        trigger_success({ message: @message.to_json, status: 200 })
      else
        trigger_failure({ message: @message.to_json, status: 400 })
      end
    end
  end
end
