module Sockets
  class ClientsController < WebsocketRails::BaseController
    before_filter :find_room

    def connected
      unless WebsocketRails["room_#{@room.id}"].is_private?
        WebsocketRails["room_#{@room.id}"].make_private
      end
    end

    def disconnected
    end

    private

    def find_room
      @room = Room.find(params[:room_id]) if params[:room_id]
    end
  end
end
