module LayoutHelper
  def title(title, opts = {})
    title = [title].flatten

    content_for :title do
      title.join(' &raquo; ').html_safe
    end
  end

  def flash_messages(opts = {})
    [:notice, :success, :info, :error, :alert, :warning].collect do |key|
      unless flash[key].blank? || !flash[key].is_a?(String)
        style = key
        style = :info if key == :notice
        style = :danger if key == :alert
        style = :danger if key == :error
        opts[:style] ||= ""
        opts[:style] += " alert-#{style}"
        opts[:style].strip!
        opts[:message] = flash[key]
        render('application/flash_message', opts)
      end
    end.join
  end

  def main_width_class
    if content_for?(:sidebar_left)
      'col-md-9'
    else
      'col-md-12'
    end
  end

  def element_for(model, tag = :article, opts = {}, &block)
    if tag.is_a?(Hash)
      opts = tag
    else
      opts[:tag] = tag
    end

    tag = opts.delete(:tag) || :article

    opts[:class] = [opts[:class]].flatten || []
    opts[:class] << 'object'
    opts[:class] << model.class.name.underscore
    opts[:class] = opts[:class].compact.join(' ')

    opts[:id] ||= dom_id(model)

    opts[:data] ||= {
      id: model.id,
      type: model.class.name.underscore
    }

    content_tag(tag, opts) do
      yield
    end
  end
end
