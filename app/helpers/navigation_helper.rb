module NavigationHelper
  def menu_link_to(label, path, options = {})
    exact_path = options.delete(:exact_path) || false
    element = options.delete(:element) || :li

    link = link_to raw(label), path, options

    if exact_path
      css_class = request.path == path ? 'active' : nil
    else
      css_class = request.path =~ /^#{path}/ ? 'active' : nil
    end

    content_tag element, link, class: css_class
  end
end
