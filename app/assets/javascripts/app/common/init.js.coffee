App.common = new Object()
App.common.init = ->
  if history && history.pushState
    $(document).on 'ajax:beforeSend', '[data-remote="true"]', (e) ->
      target = $(e.target)

      unless target.data('ignore-push-state')
        if target.is('a')
          history.pushState(null, '', e.target.href)
        else if target.is('form')
          history.pushState(null, '', target.attr('action') + '?' + target.serialize())
      $(window).bind 'popstate', ->
        $.getScript location.href

  $('a[rel=external]').each (i, el) ->
    $(el).click ->
      window.open $(el).attr('href')
      false
