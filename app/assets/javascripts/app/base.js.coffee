window.App = new Object()

@UTIL =
  exec: (controller, action) ->
    ns = App
    action = 'init' if typeof action is 'undefined'

    if controller isnt '' and ns[controller] and typeof ns[controller][action] is 'function'
      ns[controller][action]()

  init: ->
    body = document.body
    controller = body.getAttribute('data-controller')
    action = body.getAttribute('data-action')
    UTIL.exec 'common'
    UTIL.exec 'bootstrap'
    UTIL.exec 'behavior'
    UTIL.exec controller
    UTIL.exec controller, action

$(document).ready(UTIL.init)
$(document).on('page:load', UTIL.init)
$(document).ajaxSuccess(UTIL.init)
