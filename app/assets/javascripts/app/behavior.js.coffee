App.behavior = new Object()
App.behavior.init = ->
  $('[data-behavior]').each (i, el) ->
    if $(el).attr('data-behavior-active') is undefined
      behavior = $(el).data('behavior').replace(/-/g,'_').classify().toString()
      eval('new ' + behavior + 'Behavior($(el))')
      $(el).attr('data-behavior-active', 'true')
