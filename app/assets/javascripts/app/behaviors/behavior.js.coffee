class @Behavior
  EVENT: 'click'

  constructor: (@selector) ->
    return if typeof @execute != 'function'

    if @selector.data('behavior-execute')
      @selector.bind(@EVENT, $.proxy(@execute, @))
    else if @selector.find('[data-behavior-execute]').length == 1
      @selector.find('[data-behavior-execute]').bind(@EVENT, $.proxy(@execute, @))
