class @Select2AutoCompleteBehavior extends @Behavior
  constructor: (@selector) ->
    @selector.select2({
      ajax:
        url: @selector.data('search-url')
        data: @search
        results: (data, page) ->
          { results: data }
      initSelection: @initSelection
      allowClear: if @selector.data('allow-clear') != undefined then eval(@selector.data('allow-clear')) else true
      minimumInputLength: @selector.data('minimal-input') || 2
      formatResult: @formatResult
      formatSelection: @formatSelection
    })
    super

  search: (term, page) ->
    q = {}
    q[@data('search-key')] = term
    { q: q }

  formatResult: (item) ->
    HoganTemplates[@element.data('template')].render(item)

  formatSelection: (item) ->
    item.name

  initSelection: (element, callback) ->
    id = $(element).val()
    if id isnt ''
      $.ajax(element.data('init-url').replace('{{value}}', id)).done (item) ->
        callback(item)

class @Select2Behavior extends Behavior
  constructor: (@selector) ->
    @selector.select2({
      allowClear: if @selector.data('allow-clear') != undefined then eval(@selector.data('allow-clear')) else true
    })
    super
