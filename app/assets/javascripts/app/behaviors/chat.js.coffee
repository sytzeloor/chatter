class @ChatBehavior extends @Behavior
  constructor: (@selector) ->
    @log = @selector.find('textarea#messages')
    @input = @selector.find('input#message_content')

    @currentUser = null

    @roomId = @selector.data('room-id')

    url = location.hostname
    url += ':' + location.port if location.port != ''
    url += '/websocket?room_id=' + @roomId

    that = @

    @dispatcher = new WebSocketRails(url);
    @channel = @dispatcher.subscribe_private('room_' + @roomId, @successAuth, @failureAuth)

    @channel.bind 'message.welcome', (message) ->
      console.log('Welcome message received')

    @channel.bind 'message.from_history', (message) ->
      console.log('Message received from history')
      console.log(message)

    @channel.bind 'message.new', (message) ->
      that.append(message.content)

    @channel.bind 'user.joined', (user) ->
      that.append(user.name + ' joined the channel')

    @input.bind 'keypress', (e) ->
      code = e.keyCode || e.which

      if code == 13
        that.dispatcher.trigger('message.create', $(e.target).val(), that.success, that.failure)
        $(e.target).val('')
        return false

  success: (response) ->
    console.log(response.status + ': ' + response.message)

  failure: (response) ->
    console.log(response.status + ': ' + response.message)

  append: (message) ->
    content = '[' + Date().toLocaleString() + '] '
    content += $.trim(message)
    @log.val(@log.val() + "\n" + content)

  successAuth: (currentUser) ->
    @currentUser = currentUser
    # @dispatcher.trigger('user.joined', currentUser, @success, @failure);

  failureAuth: (message) ->
    console.log('Authorization failed because ' + reason.message)
