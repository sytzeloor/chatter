class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :owner, index: true
      t.string :name
      t.text :description
      t.string :state
      t.integer :members_count, default: 0

      t.timestamps null: false
    end
    add_foreign_key :rooms, :users, column: :owner_id
  end
end
