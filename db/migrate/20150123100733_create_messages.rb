class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :user, index: true
      t.references :room, index: true
      t.string :kind
      t.text :content
      t.string :state

      t.timestamps null: false
    end
    add_foreign_key :messages, :users
    add_foreign_key :messages, :rooms
  end
end
