class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.references :user
      t.references :room

      t.timestamps null: false
    end
    add_foreign_key :members, :users
    add_foreign_key :members, :rooms

    add_index :members, [:user_id, :room_id], unique: true
  end
end
