ENV['REDISTOGO_URL'] ||= 'redis://127.0.0.1:6379'

begin
  uri = URI.parse(ENV['REDISTOGO_URL'])

  Resque.redis = Redis.new(
    host: uri.host,
    port: uri.port,
    password: uri.password,
    thread_safe: true
  )
  Resque.redis.namespace = 'resque:chatter'

  require 'resque-scheduler'

  Dir["#{Rails.root}/app/workers/*.rb"].each { |file| require file }
  # Resque.schedule = YAML.load_file(Rails.root.join('config', 'resque_schedule.yml'))

  Resque.after_fork do |job|
    ActiveRecord::Base.establish_connection
    Resque.redis.client.reconnect
  end

  if Rails.env.development?
    Resque.logger = Logger.new STDOUT
    Resque.logger.level = Logger::DEBUG
  end
rescue ArgumentError => e
end
