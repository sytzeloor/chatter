require 'resque/tasks'
require 'resque/scheduler/tasks'

task 'resque:setup' => :environment do
  require 'resque'
  require 'resque-scheduler'
  ENV['QUEUES'] ||= 'critical,high,low'

  # Resque.schedule = YAML.load_file('config/resque_schedule.yml')
end

task "resque:preload" => :environment do
end

desc 'Clear pending tasks'
task 'resque:clear' => :environment do
  Resque.queues.each do |queue_name|
    puts "Clearing #{queue_name}..."
    Resque.redis.del "queue:#{queue_name}"
  end

  puts 'Clearing delayed...'
  Resque.redis.keys('delayed:*').each do |key|
    Resque.redis.del "#{key}"
  end
  Resque.redis.del 'delayed_queue_schedule'

  puts 'Clearing stats...'
  Resque.redis.set 'stat:failed', 0
  Resque.redis.set 'stat:processed', 0
end
